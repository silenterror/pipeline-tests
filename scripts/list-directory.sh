#!/bin/bash
# Description: This should contain an overall description on what
#              this script performs.
#              
#
# Primary Use Case: A once sentence description of the use case. 
#
# Manual Execution:
# docker pull -e hub.example.net/org/name_of_container:latest
# docker run -it --entrypoint /bin/bash -e FOO=foovalue hub.exxample.net/org/name_of_container:latest
# 
# ./shell-skeleton.sh --foo $FOO
# 
# Prereqs: Ensure any prereqs are listed
#
#-------------------------------------------------------------------
# OPERATIONS - tasks that the script executes
#   - Interacts with foo 
#   - Calls bar API
#-------------------------------------------------------------------
# Global Vars
#-------------------------------------------------------------------
VERBOSE=1

#-------------------------------------------------------------------
# Functions
#-------------------------------------------------------------------

# execute - runs the renewal process against each datacenter
execute() {
    list
}

# list - prints the contents of the path
list() {
    verbose "executing list"
    /usr/bin/ls -latr $LIST_PATH
}

# errexit - message and exit the script
errexit() {
    # Function for exit due to fatal program error
    # Accepts 1 arg: string containing descriptive error message
    echo "${scriptname}: ${1:-"Unknown Error"}" >&2
    exit 1
}

# signal_exit - handles signals sent to the script
signal_exit() {
    case ${1} in
        INT)
            echo "${scriptname}: Program aborted by user" >&2
            exit;;
        TERM)
            echo "${scriptname}: Program terminated" >&2
            exit;;
        *)
            errexit "${scriptname}: Terminating on unknown signal";;
    esac
}

# verbose - prints a verbose message
verbose() {
    if [ ${VERBOSE} -eq 1 ]; then
        echo "[INFO]: ${1}"
    fi
}

# -------------------------------------------------------------------
#  Start Script Execution
# -------------------------------------------------------------------
# Trap TERM, HUP, and INT signals and properly exit
trap "signal_exit TERM" TERM HUP
trap "signal_exit INT"  INT

info() {
    cat << EOF
# Check for required variables and pass in via flags. Below comments are example test values. 
#------------------------------------------------------------------------
# FOO: $FOO
#------------------------------------------------------------------------
EOF
}

execute
verbose "Process completed successfully."
exit 0

